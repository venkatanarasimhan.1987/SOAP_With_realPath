package com.VenkataNarasimhan.staxparser;

import com.VenkataNarasimhan.bean.Movie;
import com.VenkataNarasimhan.bean.output;
import com.VenkataNarasimhan.bean.userInput;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author olga
 */
@Named
@SessionScoped

public class SearchSTaX implements Serializable {

    @Inject
    output o;

    @Inject
    Movie mv;

    ArrayList timeList = new ArrayList();
    ArrayList actorList = new ArrayList();
    ArrayList directorList = new ArrayList();
    ArrayList producerList = new ArrayList();
    ArrayList writerList = new ArrayList();
    ArrayList studioList = new ArrayList();
    private List<Movie> movieList = new ArrayList<>();

    ArrayList listTimes = null;
    ArrayList listActors = null;
    ArrayList listDirectors = null;
    ArrayList listProducer = null;
    ArrayList listWriter = null;
    ArrayList listStudio = null;

    boolean bMovie = false;
    boolean bTitle = false;
    boolean bRating = false;
    boolean bTime = false;
    boolean bRealease_date = false;
    boolean bRun_time = false;
    boolean bGenre = false;
    boolean bActor = false;
    boolean bDirector = false;
    boolean bProducer = false;
    boolean bWriter = false;
    boolean bStudio = false;
    boolean isRequestMovie = false;

    private final StringBuilder xmlString = new StringBuilder();

//    String filename = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/xml/movies.xml");
    String filename="C:/Users/olga/Documents/dev/test/movies.xml";

    public void titleSearch() throws XMLStreamException, FileNotFoundException {

        actorList.clear();
        timeList.clear();
        directorList.clear();
        producerList.clear();
        writerList.clear();
        studioList.clear();

        String input = "Click";
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(filename));

            String requestedMovie = input;

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        String qName = startElement.getName().getLocalPart();
                        if (qName.equalsIgnoreCase("movie")) {
                            Iterator<Attribute> attributes = startElement.getAttributes();
                            String movieId = attributes.next().getValue();
                            if (movieId.equalsIgnoreCase(requestedMovie)) {

                                xmlString.setLength(0);

                                System.out.println("Start Element : movie");
                                System.out.println("Movie Id : " + movieId);
                                isRequestMovie = true;
                            }
                        } else if (qName.equalsIgnoreCase("title")) {
                            bTitle = true;

                        } else if (qName.equalsIgnoreCase("rating")) {
                            bRating = true;
                        } else if (qName.equalsIgnoreCase("time")) {
                            bTime = true;
                        } else if (qName.equalsIgnoreCase("release_date")) {
                            bRealease_date = true;
                        } else if (qName.equalsIgnoreCase("run_time")) {
                            bRun_time = true;
                        } else if (qName.equalsIgnoreCase("genre")) {
                            bGenre = true;
                        } else if (qName.equalsIgnoreCase("actor")) {
                            bActor = true;
                        } else if (qName.equalsIgnoreCase("director")) {
                            bDirector = true;
                        } else if (qName.equalsIgnoreCase("producer")) {
                            bProducer = true;
                        } else if (qName.equalsIgnoreCase("writer")) {
                            bWriter = true;
                        } else if (qName.equalsIgnoreCase("studio")) {
                            bStudio = true;
                        }

                        break;

                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();
                        if (bTitle && isRequestMovie) {
                            bTitle = false;
                            mv.setTitle(characters.getData());

                        }
                        if (bRating && isRequestMovie) {
                            bRating = false;

                            mv.setRating(characters.getData());
                        }
                        if (bTime && isRequestMovie) {

                            bTime = false;

                            timeList.add(characters.getData());
                            mv.setTimes(timeList);

                        }
                        if (bRealease_date && isRequestMovie) {

                            bRealease_date = false;

                            mv.setRelease_date(characters.getData());
                        }
                        if (bRun_time && isRequestMovie) {

                            bRun_time = false;

                            mv.setRun_time(characters.getData());
                        }
                        if (bGenre && isRequestMovie) {

                            bGenre = false;

                            mv.setGenre(characters.getData());
                        }

                        if (bActor && isRequestMovie) {

                            bActor = false;

                            actorList.add(characters.getData());
                            mv.setStarring(actorList);

                        }

                        if (bDirector && isRequestMovie) {

                            bDirector = false;

                            directorList.add(characters.getData());
                            mv.setDirectors(directorList);
                        }
                        if (bProducer && isRequestMovie) {

                            bProducer = false;

                            producerList.add(characters.getData());
                            mv.setProducers(producerList);
                        }
                        if (bWriter && isRequestMovie) {

                            bWriter = false;

                            writerList.add(characters.getData());

                            mv.setWriters(writerList);
                        }
                        if (bStudio && isRequestMovie) {

                            bStudio = false;

                            studioList.add(characters.getData());
                            mv.setStudios(studioList);
                        }

                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();
                        if (endElement.getName().getLocalPart().equalsIgnoreCase("movie") && isRequestMovie) {
                            System.out.println("End Element : movie");
                            isRequestMovie = false;

                            //remove
                            System.out.println(mv.getTitle());
                            System.out.println(mv.getGenre());
                            System.out.println(mv.getRating());
                            System.out.println(mv.getRelease_date());
                            System.out.println(mv.getRun_time());
                            System.out.println(mv.getTimes().toString().replace("[", "").replace("]", ""));
                            System.out.println(mv.getStarring().toString().replace("[", "").replace("]", ""));
                            System.out.println(mv.getDirectors().toString().replace("[", "").replace("]", ""));
                            System.out.println(mv.getProducers().toString().replace("[", "").replace("]", ""));
                            System.out.println(mv.getWriters().toString().replace("[", "").replace("]", ""));
                            System.out.println(mv.getStudios().toString().replace("[", "").replace("]", ""));

                        }
                        break;
                }
            }

        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }        
    }

    public void actorSTaXSearch() throws XMLStreamException, FileNotFoundException {
        movieList.clear();  // added

        timeList.clear();
        actorList.clear();
        directorList.clear();
        producerList.clear();
        writerList.clear();
        studioList.clear();

        try {

            List<Movie> movies = null;

            Movie empl = null;

            String text = "";

            XMLInputFactory factory = XMLInputFactory.newInstance();

            XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(new File(filename)));

            while (reader.hasNext()) {

                int Event = reader.next();

                switch (Event) {

                    case XMLStreamConstants.START_ELEMENT: {

                        if ("movie".equals(reader.getLocalName())) {

                            empl = new Movie();
                            listTimes = new ArrayList();
                            listActors = new ArrayList();
                            listDirectors = new ArrayList();
                            listProducer = new ArrayList();
                            listWriter = new ArrayList();
                            listStudio = new ArrayList();

                        }

//                        if ("movies".equals(reader.getLocalName())) {
//                            movieList = new ArrayList<>();
//                        }
                        break;

                    }

                    case XMLStreamConstants.CHARACTERS: {

                        text = reader.getText().trim();

                        break;

                    }

                    case XMLStreamConstants.END_ELEMENT: {

                        switch (reader.getLocalName()) {

                            case "movie": {

                                movieList.add(empl);

                                break;

                            }

                            case "title": {

                                empl.setTitle(text);

                                break;

                            }

                            case "rating": {

                                empl.setRating(text);

                                break;

                            }

                            case "release_date": {

                                empl.setRelease_date(text);

                                break;

                            }

                            case "genre": {

                                empl.setGenre(text);

                                break;

                            }
                            case "run_time": {

                                empl.setRun_time(text);

                                break;

                            }
                            case "time": {

                                listTimes.add(text);

                                empl.setTimes(listTimes);

                                break;
                            }

                            case "actor": {

                                listActors.add(text);

                                empl.setStarring(listActors);

                                break;
                            }

                            case "director": {

                                listDirectors.add(text);

                                empl.setDirectors(listDirectors);

                                break;
                            }
                            case "producer": {

                                listProducer.add(text);

                                empl.setProducers(listProducer);

                                break;
                            }
                            case "writer": {

                                listWriter.add(text);

                                empl.setWriters(listWriter);

                                break;
                            }
                            case "studio": {

                                listStudio.add(text);

                                empl.setStudios(listStudio);

                                break;
                            }

                        }

                        break;

                    }

                }

            }

            // Print all employees.
            for (Movie mov : movieList) {
//                System.out.println(mov.toString());
            }

        } //        catch (NullPointerException ne) {
        //            ne.printStackTrace();
        //        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

}
