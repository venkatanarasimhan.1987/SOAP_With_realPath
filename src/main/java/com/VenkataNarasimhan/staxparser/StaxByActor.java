package com.VenkataNarasimhan.staxparser;

import com.VenkataNarasimhan.bean.Movie;
import com.VenkataNarasimhan.bean.userInput;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;

/**
 *
 * @author olga
 */
@Named
@RequestScoped
public class StaxByActor {

    @Inject
    SearchSTaX stax;

    @Inject
    private Movie mv;

    @Inject
    userInput ui;

    List<Movie> movieList = new ArrayList<>();

//    String filename = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/xml/movies.xml");
    String filename="C:/Users/olga/Documents/dev/test/movies.xml";

    public void byActorSearch()  {

        try{
        
        stax.actorSTaXSearch();
        mv.reset();

        List<Movie> testList = new ArrayList<>();

        testList = stax.getMovieList();

        System.out.println(testList);

        for (int i = 0; i < testList.size(); i++) {

            for (int k = 0; k < testList.get(i).getStarring().size(); k++) {

                if (testList.get(i).getStarring().get(k).equalsIgnoreCase(ui.getUserInput())) {
                    mv = testList.get(i);
                    movieList.add(mv);

                }
            }
        }
        }catch(Exception e){
            e.printStackTrace();
            
        }
    }
    
        public void byDirectorSearch() throws XPathExpressionException, XMLStreamException, FileNotFoundException {

        try{
        
        stax.actorSTaXSearch();
        mv.reset();

        List<Movie> testList = new ArrayList<>();

        testList = stax.getMovieList();

        System.out.println(testList);

        for (int i = 0; i < testList.size(); i++) {

            for (int k = 0; k < testList.get(i).getDirectors().size(); k++) {

                if (testList.get(i).getDirectors().get(k).equalsIgnoreCase(ui.getUserInput())) {
                    mv = testList.get(i);
                    movieList.add(mv);

                }
            }
        }
        }catch(Exception ex){
           ex.printStackTrace();
        }

    }
    
    

    public List<Movie> getMovieList() {
        return movieList;
    }

}
