/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VenkataNarasimhan.service;

import com.VenkataNarasimhan.bean.Movie;
import com.VenkataNarasimhan.staxparser.SearchSTaX;
import java.io.FileReader;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Iterator;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author olga
 */
@WebService(serviceName = "StaxSearch")
public class StaxSearch {
    
    
    @Inject
    SearchSTaX stax;

    @Inject
    private Movie mv;
    ArrayList timeList = new ArrayList();
    ArrayList actorList = new ArrayList();
    ArrayList directorList = new ArrayList();
    ArrayList producerList = new ArrayList();
    ArrayList writerList = new ArrayList();
    ArrayList studioList = new ArrayList();

    ArrayList listTimes = null;
    ArrayList listActors = null;
    ArrayList listDirectors = null;
    ArrayList listProducer = null;
    ArrayList listWriter = null;
    ArrayList listStudio = null;

    boolean bMovie = false;
    boolean bTitle = false;
    boolean bRating = false;
    boolean bTime = false;
    boolean bRealease_date = false;
    boolean bRun_time = false;
    boolean bGenre = false;
    boolean bActor = false;
    boolean bDirector = false;
    boolean bProducer = false;
    boolean bWriter = false;
    boolean bStudio = false;
    boolean isRequestMovie = false;
 Movie temp = new Movie();
 
//   String filename = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/xml/movies.xml");
    String filename="C:/Users/Venkata/Documents/dev/test/movies.xml";

    /**
     * This is a sample web service operation
     * @param input
     * @param mode
     * @return 
     */
    @WebMethod(operationName = "stax")

    public String titleSearch(@WebParam(name = "input") String input, @WebParam(name="mode") int mode) {
      
         ArrayList<Movie> list = new ArrayList();
          String result="";
        temp.reset();
        if(mode==1){

       

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            // !!! CHANGE TO RealPath
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(filename)); 

            String requestedMovie = input;

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        String qName = startElement.getName().getLocalPart();
                        if (qName.equalsIgnoreCase("movie")) {
                            Iterator<Attribute> attributes = startElement.getAttributes();
                            String movieId = attributes.next().getValue();
                            if (movieId.equalsIgnoreCase(requestedMovie)) {

                                isRequestMovie = true;
                                        
                            }
                        } else if (qName.equalsIgnoreCase("title")) {
                            bTitle = true;

                        } else if (qName.equalsIgnoreCase("rating")) {
                            bRating = true;
                        } else if (qName.equalsIgnoreCase("time")) {
                            bTime = true;
                        } else if (qName.equalsIgnoreCase("release_date")) {
                            bRealease_date = true;
                        } else if (qName.equalsIgnoreCase("run_time")) {
                            bRun_time = true;
                        } else if (qName.equalsIgnoreCase("genre")) {
                            bGenre = true;
                        } else if (qName.equalsIgnoreCase("actor")) {
                            bActor = true;
                        } else if (qName.equalsIgnoreCase("director")) {
                            bDirector = true;
                        } else if (qName.equalsIgnoreCase("producer")) {
                            bProducer = true;
                        } else if (qName.equalsIgnoreCase("writer")) {
                            bWriter = true;
                        } else if (qName.equalsIgnoreCase("studio")) {
                            bStudio = true;
                        }

                        break;

                    case XMLStreamConstants.CHARACTERS:
                        Characters characters = event.asCharacters();
                        if (bTitle && isRequestMovie) {
                            bTitle = false;

                            temp.setTitle(characters.getData());
                        }
                        if (bRating && isRequestMovie) {
                            bRating = false;

                            temp.setRating(characters.getData());
                        }
                        if (bTime && isRequestMovie) {

                            bTime = false;

                            timeList.add(characters.getData());

                            temp.setTimes(timeList);

                        }
                        if (bRealease_date && isRequestMovie) {

                            bRealease_date = false;

                            temp.setRelease_date(characters.getData());
                        }
                        if (bRun_time && isRequestMovie) {

                            bRun_time = false;

                            temp.setRun_time(characters.getData());
                        }
                        if (bGenre && isRequestMovie) {

                            bGenre = false;

                            temp.setGenre(characters.getData());
                        }

                        if (bActor && isRequestMovie) {

                            bActor = false;

                            actorList.add(characters.getData());
                            temp.setStarring(actorList);
                        }

                        if (bDirector && isRequestMovie) {

                            bDirector = false;

                            directorList.add(characters.getData());

                            temp.setDirectors(directorList);
                        }
                        if (bProducer && isRequestMovie) {

                            bProducer = false;

                            producerList.add(characters.getData());

                            temp.setProducers(producerList);
                        }
                        if (bWriter && isRequestMovie) {

                            bWriter = false;

                            writerList.add(characters.getData());

                            temp.setWriters(writerList);
                        }
                        if (bStudio && isRequestMovie) {

                            bStudio = false;

                            studioList.add(characters.getData());

                            temp.setStudios(studioList);
                        }

                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();
                        if (endElement.getName().getLocalPart().equalsIgnoreCase("movie") && isRequestMovie) {
                            isRequestMovie = false;

                            list.add(temp);
                        }
                        break;
                }
            }

        } catch (NullPointerException ne) {
            ne.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        if(list.size()!=0){
        result= list.toString().replace("[", "").replace("]", "");
        }else{
            result="No Results";
        }

        } else if(mode==2){
             List<Movie> movieList = new ArrayList<>();
            try{
            
           
            stax.actorSTaXSearch();
        mv.reset();

        List<Movie> testList = new ArrayList<>();

        testList = stax.getMovieList();

        System.out.println(testList);

        for (int i = 0; i < testList.size(); i++) {

            for (int k = 0; k < testList.get(i).getStarring().size(); k++) {

                if (testList.get(i).getStarring().get(k).equalsIgnoreCase(input)) {
                    mv = testList.get(i);
                    movieList.add(mv);

                }
            }
        }
            }catch(Exception e){
                e.printStackTrace();
            }
            
            result=movieList.toString().replace("[", "").replace("]", "");
        }else if (mode==3){
              List<Movie> movieList = new ArrayList<>(); 
             try{
        
        stax.actorSTaXSearch();
        mv.reset();

        List<Movie> testList = new ArrayList<>();

        testList = stax.getMovieList();

        System.out.println(testList);

        for (int i = 0; i < testList.size(); i++) {

            for (int k = 0; k < testList.get(i).getDirectors().size(); k++) {

                if (testList.get(i).getDirectors().get(k).equalsIgnoreCase(input)) {
                    mv = testList.get(i);
                    movieList.add(mv);

                }
            }
        }
        }catch(Exception ex){
           ex.printStackTrace();
        }

            
            result=movieList.toString().replace("[", "").replace("]", "");
        }else{
            result="Please enter correct values";
        }
      
     return result;   
    }
}
