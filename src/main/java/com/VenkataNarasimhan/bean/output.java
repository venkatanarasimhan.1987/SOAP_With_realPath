
package com.VenkataNarasimhan.bean;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author olga
 */
@Named
@RequestScoped
public class output implements Serializable{
    
    private StringBuilder sb=new StringBuilder();

    public output() {
    }

    public StringBuilder getSb() {
        return sb;
    }

    public void setSb(StringBuilder sb) {
        this.sb = sb;
    }

 
    
}
