
package com.VenkataNarasimhan.bean;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author olga
 */
@Named
@SessionScoped
public class userInput implements Serializable{
    
    private String userInput; 

    public userInput() {
       
    }

    public String getUserInput() {
        return userInput;
    }

    public void setUserInput(String userInput) {
        this.userInput = userInput;
    }
    
}
