
package com.VenkataNarasimhan.bean;

import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author olga
 */
@Named 
@RequestScoped
public class SearchEngineBean implements Serializable{
     private int engine; 
     private int mode;
     

    public SearchEngineBean() {
        
    }

    public int getEngine() {
        return engine;
    }

    public void setEngine(int engine) {
        this.engine = engine;
    }
     
     
    public void domEngine(){
        engine=1;
    }
    
    public void staxEngine(){
        engine=2;
    }
    
    public  void saxEngine(){
        engine=3;
    }
    
    public void pageNavigation(){
        
        if(engine==1 && mode==1){
           FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "result_page.xhtml"); 
        }
        else if(engine==3 &&  mode==1){
              FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "searchByDirector.xhtml");
        }else if( engine==2 &&  mode==1){
            FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "dom_engine.xhtml");
    }else if(engine==2  && mode==3){
        FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "saxTitle.xhtml");
    }
    else if(engine==1&& mode==3){
         FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "saxActor.xhtml");
    }
        else if(engine==3&& mode==3)
        {
         FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "saxDirector.xhtml");
    }
        else if(engine==2&& mode==2)
        {
         FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "staxTitle.xhtml");
    }
        else if(engine==1&& mode==2)
        {
         FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "staxActor.xhtml");
    }else if(engine==3&& mode==2)
        {
         FacesContext.getCurrentInstance().getApplication().getNavigationHandler().handleNavigation(FacesContext.getCurrentInstance(), null, "staxDirector.xhtml");
    }
    }
    
    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }
    
    public void actorMode(){
        mode=1;
    }
    
    public void titleMode(){
        mode=2;
    }
    public void directorMode(){
        mode=3;
    }
    
}
