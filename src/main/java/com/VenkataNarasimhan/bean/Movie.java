package com.VenkataNarasimhan.bean;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;

import javax.inject.Named;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author olga
 */
@Named
@RequestScoped
public class Movie {

    String title;
    String rating;
    List<String> times;
    String release_date;
    String run_time;
    String genre;
    List<String> starring;
    List<String> directors;
    List<String> producers;
    List<String> writers;
    List<String> studios;

 
    
    public Movie() {

        times = new ArrayList<String>();
        starring = new ArrayList<String>();
        directors = new ArrayList<String>();
        producers = new ArrayList<String>();
        writers = new ArrayList<String>();
        studios = new ArrayList<String>();

    }

    public String getTitle() {
        return title;
    }
    

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<String> getTimes() {
        return times;
    }

    public void setTimes(List<String> times) {
        
        this.times = times;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getRun_time() {
        return run_time;
    }

    public void setRun_time(String run_time) {
        this.run_time = run_time;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public List<String> getStarring() {
        return starring;
    }

    public void setStarring(List<String> stars) {
        

        this.starring = stars;

    }


    public List<String> getDirectors() {
        return directors;
    }

  
    public void setDirectors(List<String> directors) {
        this.directors = directors;
    }
   
    public List<String> getProducers() {
        return producers;
    }

    public void setProducers(List<String> producers) {
        this.producers = producers;
    }

    public List<String> getWriters() {
        return writers;
    }

    public void setWriters(List<String> writers) {
        this.writers = writers;
    }

    public List<String> getStudios() {
        return studios;
    }

    public void setStudios(List<String> studios) {
        this.studios = studios;
    }

@Override
    public String toString() {
        return "Title: " + title + " \nrating: " + rating + "\ntimes: " + printTime().trim() + "\nrelease_date: " + release_date + "\nrun_time: " + run_time + "\ngenre: " + genre + "\nstars: " + printStar().trim()+ "\ndirectors: " + printDir().trim()+ "\nproducers: " + printProd().trim() + "\nwriters: " + printWriters().trim()+ "\nstudios: " + printStudio().trim()+"\n\n";
    }

    public String printDir() {
        String value = getDirectors().toString();

        return value.replace("[", "").replace("]", "").replaceFirst(",", "");
    }

    public String printStar() {
        String value = getStarring().toString();

        return value.replace("[", "").replace("]", "").replaceFirst(",", "");
    }

    public String printTime() {
        String value = getTimes().toString();

        return value.replace("[", "").replace("]", "").replaceFirst(",", "");
    }

    public String printProd() {
        String value = getProducers().toString();

        return value.replace("[", "").replace("]", "").replaceFirst(",", "");
    }

    public String printWriters() {
        String value = getWriters().toString();

        return value.replace("[", "").replace("]", "").replaceFirst(",", "");
    }

    public String printStudio() {
        String value = getStudios().toString();

        return value.replace("[", "").replace("]", "").replaceFirst(",", "");
    }
    
    public void reset(){
        starring.clear();
        directors.clear();
        studios.clear();
        writers.clear();
        producers.clear();
        title="";
        times.clear();
        release_date="";
        run_time="";
        
    }
    
}
